import events               from './events.js';


const BREAKPOINT =          480;
const DELAY =               50;

let lastWidth =             document.documentElement.clientWidth;
let timeout =               null;


window.addEventListener( 'resize', () => {

    clearTimeout( timeout );
    timeout = setTimeout(
        () => {
            const width =       document.documentElement.clientWidth;
            
            if( width <= BREAKPOINT && lastWidth > BREAKPOINT
                || width > BREAKPOINT && lastWidth <= BREAKPOINT ){
                events.emit.passBreakpoint();
            }
            lastWidth =         width;
        },
        DELAY,
    );
});
