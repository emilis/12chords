import { uniq }             from '../array/index.js';

import CHORD_TYPES          from './chord-types.js';


export const NOTES =        'C C# D D# E F F# G G# A A# B'.split( ' ' );


const NOTE_TO_NUMBER = Object.fromEntries(
    NOTES.map(( note, i ) => [
        note,
        i,
    ])
);
const NUMBER_TO_NOTE =      NOTES;
const REBASE_TYPE_PATTERNS = [
    [ /^C#/,    ...NOTES.slice( 1 ),    ...NOTES.slice( 0, 1 ) ],
    [ /^D#/,    ...NOTES.slice( 3 ),    ...NOTES.slice( 0, 3 ) ],
    [ /^F#/,    ...NOTES.slice( 6 ),    ...NOTES.slice( 0, 6 ) ],
    [ /^G#/,    ...NOTES.slice( 8 ),    ...NOTES.slice( 0, 8 ) ],
    [ /^A#/,    ...NOTES.slice( 10 ),   ...NOTES.slice( 0, 10 ) ],
    [ /^C/,     ...NOTES ],
    [ /^D/,     ...NOTES.slice( 2 ),    ...NOTES.slice( 0, 2 ) ],
    [ /^E/,     ...NOTES.slice( 4 ),    ...NOTES.slice( 0, 4 ) ],
    [ /^F/,     ...NOTES.slice( 5 ),    ...NOTES.slice( 0, 5 ) ],
    [ /^G/,     ...NOTES.slice( 7 ),    ...NOTES.slice( 0, 7 ) ],
    [ /^A/,     ...NOTES.slice( 9 ),    ...NOTES.slice( 0, 9 ) ],
    [ /^B/,     ...NOTES.slice( 11 ),   ...NOTES.slice( 0, 11 ) ],
];


const normalizeNote =       note => note.replace( /[0-9]/g, '' );
export const noteToNumber = note => NOTE_TO_NUMBER[ normalizeNote( note )];
export const numberToNote = number =>
    NUMBER_TO_NOTE[
        number >= 0
            ? number % 12
            : ( number % 12 + 12 )
    ];


export const rebaseChordByNumber = ( chord, number ) =>
    chord.map( noteToNumber )
        .map( num => num + 12 + number )
        .map( numberToNote );

const rebaseToC = chord => {
    const root = noteToNumber( chord[0] );
    return chord
        .map( noteToNumber )
        .map( num => num + 12 - root )
        .map( numberToNote );
}


const rebaseType = ( baseNote, typeString ) => {
    const partialResult =   typeString.replace( /\/C/, `/${ baseNote }` );
    const baseNum =         noteToNumber( baseNote );
    for( let i = 0; i < REBASE_TYPE_PATTERNS.length; i++ ){
        if( partialResult.match( REBASE_TYPE_PATTERNS[i][0] )){
            return partialResult.replace(
                REBASE_TYPE_PATTERNS[i][0],
                REBASE_TYPE_PATTERNS[i][baseNum + 1],
            );
        }
    }
    return partialResult;
}

export const getChordType = chord => {
    let cChord = rebaseToC( chord )
    cChord = [
        cChord[0],
        ...uniq( cChord.slice( 1 )
            .filter( note => note !== 'C' )
            .sort()
        ),
    ].join( ' ' );
    if( cChord in CHORD_TYPES ){
        return rebaseType(
            normalizeNote( chord[0] ),
            CHORD_TYPES[cChord],
        );
    } else {
        return chord.join( ' ' ) + ': ' + cChord;
    }
};

export const beautifyNotes = str =>
    str
        .replace( /#/g, '♯' )
        .replace( /b/g, '♭' );
