import events               from './events.js';


const state = {
    note:                   0,
    noteOffset:             0,
    octave:                 0,
};

export default state;


events.on.changeNote( note => {
    state.note =            note;
    state.noteOffset =
        note > 6
            ? note - 12
            : note;
    events.emit.changedNote( state.note );
});


events.on.changeOctave( octave => {
    state.octave =          octave;
    events.emit.changedOctave( state.octave );
});
