import {
    noteToNumber,
    numberToNote,
}                           from '../chorda/index.js';

import state                from './state.js';


export default note => {
    const noteNumber =      noteToNumber( note ) + state.noteOffset;
    const shiftOctave =
        noteNumber >= 12
            ? 1
        : noteNumber < 0
            ? -1
            : 0;

    return note
        .replace(
            /^[^0-9]+/,
            note =>
                numberToNote( noteNumber )
        ).replace(
            /\d$/,
            num =>
                Math.max(
                    0,
                    parseInt( num, 10 )
                        + state.octave
                        + shiftOctave,
                ),
        );
};
