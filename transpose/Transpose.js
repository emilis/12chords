import { h }                from '../edom/edom.js';
import { NOTES }            from '../chorda/index.js';
import Select               from '../select/Select.js';

import events               from './events.js';
import state                from './state.js';


const OCTAVES = {
    '-2':                   '-2',
    '-1':                   '-1',
    '+0':                   '0',
    '+1':                   '+1',
    '+2':                   '+2',
};

const getOctaveValue = octave =>
    octave < 0
        ? octave
        : `+${ octave }`;


export default () => {
    const el = h( '.com-transpose',
        Select({
            className:      'octave',
            labelPrefix:    'Octave: ',
            onChange: value =>
                events.emit.changeOctave( parseInt( value, 10 )),
            options:        OCTAVES,
            value:          getOctaveValue( state.octave ),
        }),
        Select({
            className:      'note',
            labelPrefix:    'Root note: ',
            onChange: value =>
                events.emit.changeNote( parseInt( value, 10 )),
            options:        NOTES,
            value:          state.note,
        }),
    );

    events.on.changedNote( newNote =>
        el.querySelector( '.note' )
            .doChangeValue( newNote )
    );

    events.on.changedOctave( newOctave =>
        el.querySelector( '.octave' )
            .doChangeValue( getOctaveValue( newOctave ))
    );

    return el;
}
