import BoundEmitter         from '../bound-emitter/bound-emitter.js';


export default BoundEmitter([
    'changeNote',
    'changeOctave',
    'changedNote',
    'changedOctave',
]);
