const NOTES = 'c c# d d# e f f# g g# a a# b'.split( ' ' );

const NOTE_MAP =    {};

for( let octave = 0; octave < 12; octave++ ){
    for( let note = 0; note < NOTES.length; note++ ){
        const midiNum =     note + octave * 12;
        if( midiNum > 127 ){
            break;
        }
        NOTE_MAP[ NOTES[note] + ( octave - 1 )] =   midiNum;
    }
}


export const NOTE_TO_MIDI = NOTE_MAP;

export const noteToMidi = noteStr =>
    NOTE_MAP[ noteStr.toLowerCase() ];

export default noteToMidi;
