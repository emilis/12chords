import clsi                 from '../clsi/clsi.js';
import { h }                from '../edom/edom.js';

import Button               from './Button.js';


export default ( props, ...children ) =>
    Button({
        ...props,
        className: clsi(
            'com-buttons-round',
            props.className,
        ),
    }, ...children );
