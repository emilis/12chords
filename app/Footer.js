import { h }                from '../edom/edom.js';


export default () =>
    h( 'footer.com-app-footer', `
      <p>You can reach me at: emilis [at] tonnetz.eu.</p>
      <p><a href="https://codeberg.org/emilis/12chords">Source code</a></p>
    `);
