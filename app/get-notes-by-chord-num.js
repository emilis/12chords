import { getCurrentPreset } from '../presets/state.js';
import transposeNote        from '../transpose/transpose-note.js';


export default num =>
    getCurrentPreset()[ num ].map( transposeNote );
