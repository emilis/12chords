import '../chord-player/keyboard-adapter.js';
import '../presets/local-storage-adapter.js';
import '../synth/adapter.js';
import '../theme/adapter.js';

import App                  from './App.js';


export default App();
