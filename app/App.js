import { h }                from '../edom/edom.js';

import ChordPlayer          from '../chord-player/ChordPlayer.js';
import InstrumentSelector   from '../instrument-selector/InstrumentSelector.js';
import Keyboard             from '../keyboard/Keyboard.js';
import PresetsSelector      from '../presets/Selector.js';
import SynthIosFix          from '../synth/IosFix.js';
import Transpose            from '../transpose/Transpose.js';

import Footer               from './Footer.js';


export default () =>
    h( '.com-app', [
        PresetsSelector(),
        Keyboard(),
        ChordPlayer(),
        Transpose(),
        InstrumentSelector(),
        Footer(),
        SynthIosFix(),
    ]);
