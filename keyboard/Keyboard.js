import chordPlayerEvents    from '../chord-player/events.js';
import clsi                 from '../clsi/clsi.js';
import { eqs, eqsa, h }     from '../edom/edom.js';
import {
    getCssVar,
    getValueNumAndUnit,
}                           from '../css/index.js';
import getNotesByChordNum   from '../app/get-notes-by-chord-num.js';
import { NOTES }            from '../chorda/index.js';
import themeEvents          from '../theme/events.js';
import transposeEvents      from '../transpose/events.js';
import transposeState       from '../transpose/state.js';


const ALL_NOTES =           new Array( 128 );

for( let oct = 0; oct < 11; oct++ ){
    for( let n = 0; n < NOTES.length; n++ ){
        const midiNum =     n + oct * NOTES.length;
        if( midiNum >= ALL_NOTES.length ){
            break;
        }
        ALL_NOTES[midiNum] =    NOTES[n] + ( oct - 1 );
    }
}


const getClassName = note =>
    clsi(
        note[1] === '#'
            ? 'is-black'
            : 'is-white',
        note[0],
        `octave-${ note.match( /-?\d+/ )[0] }`,
    );

const getNoteSelector = note =>
    `.note.${
        getClassName( note )
            .replace( / /g, '.' )
    }`;

const getTranslate = ({ noteOffset, octave }) => {
    const [ noteWidth, unit ] =
        getValueNumAndUnit( getCssVar( '--keyboard-note-width' ));

    return `translateX(${
        7 * noteWidth
        - noteWidth * noteOffset
        - 48 * noteWidth
        - 12 * noteWidth * octave
    }${ unit })`;
};


export default () => {
    const el = h( '.com-keyboard',
        h( '.all-notes', {
            style: {
                transform:      getTranslate( transposeState ),
            },
        }, ALL_NOTES.map( noteName =>
                h( '.note', {
                    className:  getClassName( noteName ),
                    title:      noteName,
                })
            ),
        ),
    );

    chordPlayerEvents.combine(
        chordPlayerEvents.on.keyDownChord,
        chordPlayerEvents.on.touchStartChord,
        chordNum => {
            const notes =       getNotesByChordNum( chordNum );

            for( const activeNote of eqsa( el, '.note.is-active' )){
                activeNote.classList.remove( 'is-active' );
            }
            for( const note of notes ){
                eqs( el, getNoteSelector( note ))
                    .classList.add( 'is-active' );
            }
        },
    );

    themeEvents.combine(
        themeEvents.on.passBreakpoint,
        transposeEvents.on.changedNote,
        transposeEvents.on.changedOctave,
        () =>
            eqs( el, '.all-notes' ).style.transform =
                getTranslate( transposeState ),
    );

    return el;
}
