export const range = n =>
    [ ...Array( n ).keys() ];

export const uniq = arr =>
    [ ...new Set( arr )];
