import PROGRAM_NAMES        from '../synth/program-names.js';

import events               from './events.js';


const MAX_PROGRAMS =        PROGRAM_NAMES.length;


const state = {
    currentInstrument:      16,
};
export default state;


events.on.change( newInstrument => {
    state.currentInstrument =   newInstrument;
    events.emit.changedInstrument( state.currentInstrument );
});


events.on.clickNext(() => {

    state.currentInstrument = ( state.currentInstrument + 1 ) % MAX_PROGRAMS;
    events.emit.changedInstrument( state.currentInstrument );
});

events.on.clickPrevious(() => {

    state.currentInstrument =
        state.currentInstrument 
            ? state.currentInstrument - 1
            : MAX_PROGRAMS - 1;
    events.emit.changedInstrument( state.currentInstrument );
});

