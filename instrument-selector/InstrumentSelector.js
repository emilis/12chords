import PROGRAM_NAMES            from '../synth/program-names.js';
import SelectWithNextPrevious   from '../select/WithNextPrevious.js';

import events                   from './events.js';
import state                    from './state.js';


export default () => {
    const el = SelectWithNextPrevious({
        onChange: newValue =>
            events.emit.change( parseInt( newValue, 10 )),
        onClickNext:        events.emit.clickNext,
        onClickPrevious:    events.emit.clickPrevious,
        options:            PROGRAM_NAMES,
        value:              state.currentInstrument,
    });

    events.on.changedInstrument( newInstrument =>
        el.doChangeValue( newInstrument )
    );

    return el;
}
