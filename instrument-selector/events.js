import BoundEmitter         from '../bound-emitter/bound-emitter.js';


export default BoundEmitter([
    'change',
    'changedInstrument',
    'clickNext',
    'clickPrevious',
]);
