export const getCssVar = ( varName, el = document.documentElement ) =>
    getComputedStyle( el ).getPropertyValue( varName );


export const getValueNumAndUnit = cssValue => {
    const match =   cssValue.trim().match( /^([.0-9]+)([a-z]+)$/ );
    if( ! match ){
        return [ 0, '' ];
    } else {
        const [ _, number, unit ] = match;
        return [
            parseFloat( number, 10 ),
            unit,
        ];
    }
};
