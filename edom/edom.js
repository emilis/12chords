export const eqs = ( el, query ) =>
    el.querySelector( query );

export const eqsa = ( el, query ) => [
    ...( el.querySelectorAll( query ) || [] ),
];

export const qs =       eqs.bind( null, document );
export const qsa =      eqsa.bind( null, document );


export const sanitize = str =>
    str?.replace
        ? str.replace( /</g, '&lt;' )
        : str;


export const appendChildren = ( el, children ) => {
    if( children && children.length ){

        for( const child of children.flat( Infinity )){
            if( child ){
                if( child.isSameNode ){
                    el.appendChild( child );
                } else {
                    el.innerHTML += child.toString();
                }
            } else if( child === 0 ){
                el.innerHTML += child.toString();
            }
        }
    }

    return el;
}

export const replaceChildren = ( el, newChildren ) => {
    el.innerHTML =          '';
    return appendChildren( el, newChildren );
}


export function h( selector, ...children ){

    const [ name, ...classNames ] = selector.split( '.' );

    const el =                      document.createElement( name || 'div' );

    const hasClasses =              classNames.length;
    const hasAttributes = (
        children
        && children.length
        && typeof children[0] === 'object'
        && !( children[0] instanceof Array )
        && !children[0].isSameNode
    );

    if( hasClasses || hasAttributes ){

        const attr =                {};

        if( hasAttributes ){
            Object.assign( attr, children.shift() );
        }

        if( hasClasses ){
            attr.className =        classNames.join( ' ' ) + ' ' + ( attr.className || '' );
        }

        for( const key in attr ){
            if( key === 'style' ){
                for( const styleKey in attr[key] ){
                    el.style[styleKey] =    attr[key][styleKey];
                }
            } else {
                el[key] =           attr[key];
            }
        }
    }

    appendChildren( el, children );

    return el;
}
