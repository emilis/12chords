export default class EventEmitter {
    constructor() {

        this.handlers =     {};
    }

    emit( eventName, ...args ){

        if( ! this.handlers[eventName] ){
            return;
        }
        for( const handlerFn of this.handlers[eventName] ){
            try {
                handlerFn( ...args );
            } catch( err ){
                console.error( err );
            }
        }
    }

    off( eventName, handlerFn ){

        this.handlers[eventName] =  this.handlers[eventName] || [];

        this.handlers[eventName] = 
            this.handlers[eventName].filter( fn =>
                fn !== handlerFn
            );
        return handlerFn;
    }

    on( eventName, handlerFn ){

        this.handlers[eventName] =  this.handlers[eventName] || [];

        return this.handlers[eventName].push( handlerFn );
    }

    once( eventName, handlerFn ){

        const onceFn = ( ...args ) => {
            handlerFn( ...args );
            this.off( eventName, onceFn );
        };
        return this.on( eventName, onceFn );
    }
}
