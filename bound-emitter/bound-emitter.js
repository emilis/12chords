import EventEmitter         from './event-emitter.js';


export default function BoundEmitter( eventNames ){

    const emitter =         new EventEmitter;

    const api = {

        combine: ( ...functions ) => {
            const handler = functions.pop();
            return functions.map( fn =>
                fn( handler )
            );
        },
        emitter,
        emit:               {},
        off:                {},
        on:                 {},
        once:               {},
    };

    for( const name of eventNames ){
        api.emit[name] =    emitter.emit.bind( emitter, name );
        api.off[name] =     emitter.off.bind( emitter, name );
        api.on[name] =      emitter.on.bind( emitter, name );
        api.once[name] =    emitter.once.bind( emitter, name );
    }

    return api;
}
