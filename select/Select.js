import { h, sanitize }      from '../edom/edom.js';


const getLabel = value =>
    sanitize( value ) + '&nbsp;<b>&blacktriangledown;</b>';


export default ({
    className,
    labelPrefix =           '',
    onChange,
    options,
    value
}) => {
    const strValue =        '' + value;
    const el =
        h( '.com-select', { className },
            h( 'select', {
                onchange: evt => {
                    onChange?.( evt.target.value );
                },
                value:      strValue,
            }, Object.entries( options ).map(([ optionValue, label ]) =>
                h( 'option', {
                    value:      optionValue,
                    selected:   optionValue === strValue,
                }, label )
            )),
            h( '.label',
                getLabel( labelPrefix + options[value] ),
            ),
        );

    el.doChangeValue = newValue => {
        el.querySelector( 'select' ).value =      newValue;
        el.querySelector( '.label' ).innerHTML =
            getLabel( labelPrefix + options[newValue] );
    }

    return el;
};
