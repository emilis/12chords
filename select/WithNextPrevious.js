import { h }                from '../edom/edom.js';
import RoundButton          from '../buttons/Round.js';

import Select               from './Select.js';


export default ({
    className,
    onChange,
    onClickNext,
    onClickPrevious,
    options,
    value,
}) => {
    const el = h( '.com-select-with-next-previous',
        RoundButton({
            className:          'previous',
            onclick:            onClickPrevious,
        },  '«' ),
        Select({
            className:          'select',
            onChange,
            options,
            value,
        }),
        RoundButton({
            className:          'next',
            onclick:            onClickNext,
        },  '»' ),
    );

    el.doChangeValue =          el.querySelector( '.select' ).doChangeValue;

    return el;
};
