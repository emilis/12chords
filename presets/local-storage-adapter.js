import events               from './events.js';
import {
    getCurrentValue,
    getNames,
}                           from './state.js';
import presets              from './presets.js';


const ITEM_NAME =           'presets/preset';
const PRESETS_LENGTH =      getNames().length;


try {
    const storedPreset =        parseInt( localStorage.getItem( ITEM_NAME ), 10 );

    if( ! Number.isNaN( storedPreset )
       && storedPreset > 0
       && storedPreset < PRESETS_LENGTH
    ){
        events.emit.change( storedPreset );
    }


    events.on.changedPreset( () => {
        localStorage.setItem( ITEM_NAME, getCurrentValue() );
    });
} catch( err ){
    console.error( 'localStorage not supported' );
    console.error( err );
}
