import SelectWithNextPrevious   from '../select/WithNextPrevious.js';

import events                   from './events.js';
import {
    getCurrentValue,
    getNames,
}                               from './state.js';


export default () => {
    const el = SelectWithNextPrevious({
        onChange: newValue =>
            events.emit.change( parseInt( newValue, 10 )),
        onClickNext:        events.emit.clickNext,
        onClickPrevious:    events.emit.clickPrevious,
        options:            getNames(),
        value:              getCurrentValue(),
    });

    events.on.changedPreset(() =>
        el.doChangeValue( getCurrentValue() )
    );

    return el;
}
