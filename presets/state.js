import events               from './events.js';
import presets              from './presets.js';


const keys =                Object.keys( presets );

let keyIndex =              0;


export const getCurrentPreset = () =>
    presets[keys[keyIndex]];

export const getCurrentValue = () =>
    keyIndex;


export const getNames = () =>
    keys;


events.on.change( value => {
    keyIndex =              value;
    events.emit.changedPreset( getCurrentPreset() );
});


events.on.clickNext(() => {
    keyIndex =              ( keyIndex + 1 ) % keys.length;
    events.emit.changedPreset( getCurrentPreset() );
});

events.on.clickPrevious(() => {
    keyIndex =
        keyIndex === 0
            ? keys.length - 1
            : keyIndex - 1;
    events.emit.changedPreset( getCurrentPreset() );
});
