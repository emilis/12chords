import BoundEmitter         from '../bound-emitter/bound-emitter.js';


export default BoundEmitter([
    'change',
    'changedPreset',
    'clickNext',
    'clickPrevious',
]);
