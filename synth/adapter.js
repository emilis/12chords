import chordPlayerEvents        from '../chord-player/events.js';
import getNotesByChordNum       from '../app/get-notes-by-chord-num.js';
import instrumentSelectorEvents from '../instrument-selector/events.js';
import instrumentSelectorState  from '../instrument-selector/state.js';
import { noteToMidi }           from '../midi-note/note-to-midi.js';

import synth                    from './synth.js';


const CHANNEL =             0;
const DEFAULT_DURATION =    500;
const DEFAULT_VELOCITY =    110;


synth.setProgram( CHANNEL, instrumentSelectorState.currentInstrument );


chordPlayerEvents.combine(
    chordPlayerEvents.on.keyDownChord,
    chordPlayerEvents.on.touchStartChord,
    async chordNum => {

        if( synth.audioContext.state === 'suspended' ){
            await synth.audioContext.resume();
        }
        getNotesByChordNum( chordNum )
            .map( noteToMidi )
            .forEach( midiNote =>
                synth.noteOn( CHANNEL, midiNote, DEFAULT_VELOCITY )
            );
    },
);

chordPlayerEvents.combine(
    chordPlayerEvents.on.keyUpChord,
    chordPlayerEvents.on.touchEndChord,
    chordNum => {

        getNotesByChordNum( chordNum )
            .map( noteToMidi )
            .forEach( midiNote =>
                synth.noteOff( CHANNEL, midiNote )
            );
    },
);

instrumentSelectorEvents.on.changedInstrument( instrument =>

    synth.setProgram( CHANNEL, instrument )
);
