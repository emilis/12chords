import { h }                from '../edom/edom.js';


export default () =>
    h( 'audio', {
        src:                './synth/empty.mp3',
    });
