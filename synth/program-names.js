import synth                from './synth.js';


export default synth.program.map(
    ( prog, i ) =>
        `${ i + 1 }. ${ prog.name }`
);
