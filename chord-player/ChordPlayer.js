import {
    beautifyNotes,
    getChordType,
    rebaseChordByNumber,
}                           from '../chorda/index.js';
import Button               from '../buttons/Button.js';
import { eqsa, h }          from '../edom/edom.js';
import { getCurrentPreset } from '../presets/state.js';
import {
    ON_TOUCH_END,
    ON_TOUCH_START,
}                           from '../browser/index.js';
import presetsEvents        from '../presets/events.js';
import transposeEvents      from '../transpose/events.js';
import transposeState       from '../transpose/state.js';

import events               from './events.js';


const getChordName = chord =>
    beautifyNotes(
        getChordType(
            rebaseChordByNumber( chord, transposeState.note )
        )
    );

const getChordTitle = chord =>
    rebaseChordByNumber( chord, transposeState.note )
        .join( ' ' );


export default () => {
    const el = h( '.com-chord-player',
        getCurrentPreset().map(( chord, i ) =>
            Button({
                className:  'chord',
                onmouseout: () =>
                    events.emit.touchEndChord( i ),
                [ON_TOUCH_END]: () =>
                    events.emit.touchEndChord( i ),
                [ON_TOUCH_START]: () =>
                    events.emit.touchStartChord( i ),
                title:              getChordTitle( chord ),
            }, getChordName( chord ))
        ),
    );

    events.combine(
        events.on.keyDownChord,
        events.on.touchStartChord,
        chordNum =>
            eqsa( el, '.chord' )[chordNum]
                .classList.add( 'is-active' ),
    );

    events.combine(
        events.on.keyUpChord,
        events.on.touchEndChord,
        chordNum =>
            eqsa( el, '.chord' )[chordNum]
                .classList.remove( 'is-active' ),
    );

    presetsEvents.combine(
        presetsEvents.on.changedPreset,
        transposeEvents.on.changedNote,
        () => {
            const currentList =     getCurrentPreset();
            eqsa( el, '.chord' ).forEach(( button, i ) => {
                button.innerHTML =  getChordName( currentList[i] );
                button.title =      getChordTitle( currentList[i] );
            });
        },
    );

    return el;
}
