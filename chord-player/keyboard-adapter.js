import events               from './events.js';


const KEYS = {
    KeyQ:   9,  //  A   
    KeyW:   4,  //  E
    KeyE:   11, //  B
    KeyR:   6,  //  F#
    KeyA:   5,  //  F  
    KeyS:   0,  //  C  
    KeyD:   7,  //  G
    KeyF:   2,  //  D  
    KeyZ:   1,  //  C# 
    KeyX:   8,  //  G# 
    KeyC:   3,  //  D#
    KeyV:   10, //  A#
};

const isSimple = evt => ! (
    evt.altKey
    || evt.ctrlKey
    || evt.isComposing
    || evt.metaKey
    || evt.shiftKey
);



window.addEventListener( 'keydown', evt => {
    if( evt.code in KEYS
        && isSimple( evt )
        && ! evt.repeat
    ){
        events.emit.keyDownChord( KEYS[evt.code] );
    }
});

window.addEventListener( 'keyup', evt => {
    if( evt.code in KEYS
       && isSimple( evt )
    ){
        events.emit.keyUpChord( KEYS[evt.code] );
    }
});
