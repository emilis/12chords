import BoundEmitter         from '../bound-emitter/bound-emitter.js';


export default BoundEmitter([
    'keyDownChord',
    'keyUpChord',
    'touchEndChord',
    'touchStartChord',
]);
