# 12Chords.EU

A chord player to help you build chord progressions.

Open http://12chords.eu/ to use it.

## Looking for help

Please suggest new chord presets by sending them to emilis@tonnetz.eu or creating a PR to _presets/presets.js_

## Development

Start a local web server, e.g.:

```
npx http-server .
```

App startup is in _app/main.js_.
CSS files should be manually added to _index.html_.

No bundling, compiling or pre-processing is used for the source code.

## License

Copyright 2022 Emilis Dambauskas emilis@tonnetz.eu

This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE.txt for details.

